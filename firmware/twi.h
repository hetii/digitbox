#include <avr/io.h>

#define USE_HARDWARE 1

#define HWPIN_SDA C,4
#define HWPIN_SCL C,5

#define ACK 1
#define NOACK 0

void twi_init(void);
void twi_start(void);
void twi_stop(void);
void twi_write(char data);
char twi_read(char ack);
